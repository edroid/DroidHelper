package com.edroid.droidhelper.pc.view;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import net.miginfocom.swing.MigLayout;

import com.edroid.droidhelper.pc.model.Res;

public class ErrorDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private String exceptionString;
	
	
	public ErrorDialog(Throwable ex) {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("出错啦 ⊙﹏⊙b");
		setIconImage(Res.getImage("/res/drawable/icon.png"));
		
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosed(WindowEvent e) {
				System.exit(1);
			}
		});
		
		StringWriter w = new StringWriter();
		if (ex.getClass() == RuntimeException.class && ex.getCause() != null)
			ex = ex.getCause();
		ex.printStackTrace(new PrintWriter(w));
		exceptionString = w.toString();
		
		getContentPane().setLayout(new BorderLayout());
		
		JTextPane l = new JTextPane();
		l.setText(exceptionString);
		getContentPane().add(l, BorderLayout.CENTER);
		
		{
			JPanel panel = new JPanel(new MigLayout());
//			panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			getContentPane().add(panel, BorderLayout.SOUTH);
			
			JButton button = new JButton("提交错误报告");
			button.setIcon(new ImageIcon(getClass().getResource("/res/drawable/ic_send.png")));
			button.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					uploadException();
				}
			});
			panel.add(button, "cell 0 0, grow");
			
			JButton button2 = new JButton("复制到剪切板");
			button2.setIcon(new ImageIcon(getClass().getResource("/res/drawable/ic_copy.png")));
			button2.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					 StringSelection stsel = new StringSelection(exceptionString);
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stsel, stsel);
				}
			});
			panel.add(button2, "cell 1 0, grow");
		}
		
		pack();
		setLocationRelativeTo(null);
		setAlwaysOnTop(true);
//		setAlwaysOnTop(false);
	}

	private void uploadException() {
		
	}
}
