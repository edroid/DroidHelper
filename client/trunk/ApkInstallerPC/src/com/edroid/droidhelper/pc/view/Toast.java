package com.edroid.droidhelper.pc.view;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;
import javax.swing.SwingConstants;

public class Toast extends JWindow {
	/**
	 * 
	 */
	private static final long serialVersionUID = 10L;
	
	private JLabel label;

	
	public Toast(String text) {
		initialize();
		setText(text);
	}

	public void setText(String text) {
		label.setText(text);
		pack();
		setLocationRelativeTo(null);
	}

	private void initialize() {
		label = new JLabel("", 
				new ImageIcon(getClass().getResource("/res/drawable/icon.png")), 
				SwingConstants.CENTER);
		
		setLayout(new BorderLayout());
		label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		add(label, BorderLayout.CENTER);
	}
	
	public void show() {
		setVisible(true);
//		return this;
	}
	
	public static Toast make(String text) {
		return new Toast(text);
	}
}
