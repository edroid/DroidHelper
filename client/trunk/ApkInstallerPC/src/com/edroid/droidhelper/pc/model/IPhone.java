package com.edroid.droidhelper.pc.model;

import com.android.ddmlib.IDevice;
import com.edroid.droidhelper.pc.model.Phone.FetchScreenshotCallback;

public interface IPhone {

	public void installApk(String apkPath) throws Exception;
	
	public void onStateChanged(IDevice iDevice, int mask);
	
	public void onDisconnect();
	
	public boolean isAvailable();
	
	public void runShellCommond(ShellCommand cmd);
	
	public String getProp(String key);
	
	public String getModel();
	
	public String getManufacturer();
	
	public String getImei();
	
	public String getHardware();
	
	public String getOsVer();

	public String getHostName();
	
	public void fetchScreenshot(FetchScreenshotCallback cb);
	
	public void startInstallTask();

	/**
	 * 停止装机任务
	 * 
	 * @param waitForFinish 等待装机线程接触，并记录日志
	 */
	public void stopInstallTask(boolean waitForFinish);
}
