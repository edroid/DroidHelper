package com.edroid.droidhelper.pc.bean;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


public class InstallLogBean {
	public String _id;

	public String userId;

	public String imei;
	public String model;
	public String manufacturer;
	public String hardware;
	public String osver;
	public String hostname;

	public String toolType;
	public String toolName;
	public String toolMac;
	public String toolIp;
	public String toolOs;

	public String appCount;
	public String appList;

	public String time;
	public String timeMills;
	
	
	public InstallLogBean() {
	}
	
	public InstallLogBean(JSONObject jsonObject) {
		try {
			Field[] fields = getClass().getDeclaredFields();
			for(Field f : fields) {
				f.setAccessible(true);
				
				f.set(this, jsonObject.opt(f.getName()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JSONObject toJsonObject() {
		JSONObject json = new JSONObject();
		try {
			Field[] fields = getClass().getDeclaredFields();
			for(Field f : fields) {
				f.setAccessible(true);
				
				if("_id".equals(f.getName())) {
					continue;
				}
				
				/*if("appList".equals(f.getName())) {
					if (!TextUtils.isEmpty(appList)) {
						String[] ss = appList.split(",");
						JSONArray array = new JSONArray();
						for (String e : ss) {
							array.put(e);
						}
						json.put(f.getName(), array);
					}
				} else */{
					json.put(f.getName(), f.get(this));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
	
	public static List<InstallLogBean> parse(String s) {
		List<InstallLogBean> list = null;
		
		try {
			JSONArray array = new JSONArray(s);
			final int N = array.length();
			list = new ArrayList<InstallLogBean>(); 
			for(int i=N-1; i >= 0; i--) {
				JSONObject jsonObject = array.getJSONObject(i);
				
				list.add(new InstallLogBean(jsonObject));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
}
