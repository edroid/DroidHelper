package com.edroid.droidhelper.pc.model;

import java.util.prefs.Preferences;

/**
 * 程序配置
 * 
 * @author 建宾
 *
 */
public class AppPrefes {
	private static Preferences gPreferences;
	
	public static Preferences getPreferences() {
		// HKEY_LOCAL_MACHINE/Software/JavaSoft/prefs下写入注册表值.   
		if(gPreferences == null) {
			gPreferences = Preferences.userRoot().node(AppGlobal.PREFS_NAME);
		}
		
		return gPreferences;
	}
	
	public static void applay() {
//		if(gPreferences != null)
//			try {
//				gPreferences.sync();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
	}
	

	public static String get(String key, String def) {
		Preferences pre = getPreferences();

		return pre.get(key, def);
	}
	
	public static boolean getBoolean(String key, boolean def) {
		return getPreferences().getBoolean(key, def);
	}
	
	public static void putBoolean(String key, boolean value) {
		Preferences pre = getPreferences();
		pre.putBoolean(key, value);
	}
	
	public static void put(String key, String value) {
		Preferences pre = getPreferences();
		
		pre.put(key, value);
	}
}
