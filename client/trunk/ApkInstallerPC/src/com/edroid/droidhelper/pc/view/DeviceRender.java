package com.edroid.droidhelper.pc.view;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;

import javax.swing.border.EmptyBorder;

import com.edroid.droidhelper.pc.model.Phone;
import com.edroid.droidhelper.pc.model.IInstallTask;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JTextPane;

public class DeviceRender extends JPanel implements IInstallTask.OnProgUpdateListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 100L;
	
	private JLabel lblName;
	private JLabel lblModel;
	private JLabel lblImei;
	private JLabel lblCurInstall;
	private JLabel lblProg;
	private JProgressBar progressBar;
	private Phone device;
	private JLabel label_4;
	private JLabel lblState;
	private JTextPane textPane;
	
	private StringBuilder sb = new StringBuilder();
	

	/**
	 * Create the panel.
	 */
	public DeviceRender(Phone device) {
		this.device = device;
		
		setBorder(new EmptyBorder(8, 8, 8, 8));
		setBackground(Color.WHITE);
		
		setLayout(new MigLayout("wrap", "[right]15[grow]", "[]10[]8[]8[]8[]8[]8[grow]"));
		
		label_4 = new JLabel("设备状态：");
		label_4.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		add(label_4, "cell 0 0");
		
		lblState = new JLabel();
		lblState.setForeground(Color.RED);
		lblState.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		add(lblState, "cell 1 0");
		
		JLabel label = new JLabel("设备名：");
		add(label, "cell 0 1");
		
		lblName = new JLabel();
		add(lblName, "cell 1 1,growx");
		
		JLabel label_2 = new JLabel("厂商机型：");
		add(label_2, "cell 0 2");
		
		lblModel = new JLabel();
		add(lblModel, "cell 1 2");
		
		JLabel label_1 = new JLabel("串号：");
		add(label_1, "cell 0 3");
		
		lblImei = new JLabel();
		add(lblImei, "cell 1 3");
		
		JLabel label_3 = new JLabel("进度：");
		add(label_3, "cell 0 4");
		
		lblCurInstall = new JLabel();
		lblCurInstall.setForeground(Color.BLUE);
		add(lblCurInstall, "cell 1 4");
		
		lblProg = new JLabel("0/0");
		lblProg.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblProg, "cell 0 5");
		
		progressBar = new JProgressBar();
		add(progressBar, "cell 1 5,growx");
		
		textPane = new JTextPane();
		JScrollPane scrollPane = new JScrollPane(textPane);
		scrollPane.setPreferredSize(new Dimension(300, 0));
		add(scrollPane, "cell 0 6 2 1,grow");
		
		textPane.setText("");

		//////////////
		device.setOnProgUpdateListener(this);
	}
	
	public void showDeviceInfo() {
		if(device.isAvailable()) {
			lblState.setForeground(Color.BLUE);
			lblState.setText("连接正常");
		} else {
			lblState.setForeground(Color.RED);
			lblState.setText("连接错误 " + device.getDeviceState());
		}
		
		lblModel.setText(device.getManufacturer() + "-" + device.getModel() + "-" + device.getHardware());
		lblImei.setText(device.getImei());
		lblName.setText(device.getName());
	}

	@Override
	public void onProgUpdate(IInstallTask task, final int total, final int cur, final String msg) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				progressBar.setMaximum(total);
				progressBar.setValue(cur);
				
				sb.append(msg).append('\n');
				textPane.setText(sb.toString());
				
				lblProg.setText("" + cur + "/" + total);
//				lblCurInstall.setText(msg);
			}
		});
	}
}
