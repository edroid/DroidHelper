package com.edroid.droidhelper.pc.model;

public interface DeviceChangeListener {

	/**
	 * 设备连接
	 * 
	 * @param device
	 */
	public void onDeviceAttached(Phone device);

	/**
	 * 设备断开
	 * 
	 * @param device
	 */
	public void onDeviceDetached(Phone device);

	/**
	 * 设备状态改变
	 * 
	 * @param device
	 * @param available
	 */
	public void onDeviceStateChanged(Phone device, boolean available);
}
