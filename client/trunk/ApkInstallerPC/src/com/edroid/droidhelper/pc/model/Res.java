package com.edroid.droidhelper.pc.model;

import java.awt.Image;
import java.awt.Toolkit;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;


/**
 * 资源管理器
 * 
 * @author yichou 2015-1-14
 *
 */
public class Res {
	private static final Map<String, WeakReference<Image>> pool 
		= new HashMap<String, WeakReference<Image>>();
	

	public static Image getImage(String name) {
		WeakReference<Image> r = pool.get(name);
		if(r != null && r.get() != null)
			return r.get();
		
		Image image = Toolkit.getDefaultToolkit().getImage(Res.class.getResource(name));
		pool.put(name, new WeakReference<Image>(image));
		
		return image;
	}
	
	public static void recycle() {
		
	}
}
