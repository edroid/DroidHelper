package com.edroid.droidhelper.pc.view;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import com.edroid.droidhelper.pc.model.Res;

public class AboutDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7L;
	private final JPanel contentPanel = new JPanel();


	/**
	 * Create the dialog.
	 */
	public AboutDialog() {
		setTitle("关于");
		setIconImage(Res.getImage("/res/drawable/icon.png"));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 450, 300);
		setLocationRelativeTo(null);
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(20, 15, 15, 15));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JTextPane txtCopyright = new JTextPane();
			txtCopyright.setText("亿卓网络科技版权所有\r\n\r\nCopyright 2015");
			txtCopyright.setEditable(false);
			contentPanel.add(txtCopyright, BorderLayout.CENTER);
		}
	}

}
