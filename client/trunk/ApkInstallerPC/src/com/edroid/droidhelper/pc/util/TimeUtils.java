package com.edroid.droidhelper.pc.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * 时间、日期、本地化 相关工具类
 * 
 * @author Yichou 2013-6-24
 *
 */
public final class TimeUtils {
	
	static final String DEF_DATE_TIME_TEMPLATE = "yyyy-MM-dd HH:mm:ss";
	static final String DEF_DATE_TEMPLATE = "yyyy-MM-dd";
	static final String DEF_TIME_TEMPLATE = "HH:mm:ss";
	
	/**
	 * @return   yyyy-MM-dd HH:mm:ss
	 */
	public static String getDateTimeNow() {
		return getDateTimeNow(DEF_DATE_TIME_TEMPLATE);
	}

	/**
	 * 按模板格式化
	 */
	public static String getDateTimeNow(String template) {
		SimpleDateFormat sdf = new SimpleDateFormat(template);
		return sdf.format(new Date());
	}
	
	/**
	 * @return   yyyy-MM-dd HH:mm:ss
	 */
	public static String formatDate(long ms) {
		return formatDate(ms, DEF_DATE_TIME_TEMPLATE);
	}
	
	public static String getDateTime(long ms) {
		return formatDate(ms, DEF_DATE_TIME_TEMPLATE);
	}

	public static String getTime(long ms) {
		return formatDate(ms, DEF_TIME_TEMPLATE);
	}

	public static String getDate(long ms) {
		return formatDate(ms, DEF_DATE_TEMPLATE);
	}
	
	public static String formatDate(long ms, String template) {
		// 取系统时间
		SimpleDateFormat format = new SimpleDateFormat(template);
		return format.format(new Date(ms));
	}
	
	/**
	 * @return 形式  12:23:32
	 */
	public static String getTimeNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(new Date());
	}
	
	public static int getDayOfYear() {
		return Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
	}
	
	public static int getHourOfDay() {
		return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	}
	
	public static long currentTimeMillis() {
		return System.currentTimeMillis();
	}
}
