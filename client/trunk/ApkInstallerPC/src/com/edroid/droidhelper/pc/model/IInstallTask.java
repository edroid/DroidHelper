package com.edroid.droidhelper.pc.model;

public interface IInstallTask {
	
	public interface OnProgUpdateListener {
		
		public void onProgUpdate(IInstallTask task, int total, int cur, String msg);
	}
	
	public void setOnProgUpdateListener(OnProgUpdateListener listener);

	/**
	 * 获取进度总数
	 * @return
	 */
	public int getProgTotal();

	/**
	 * 获取当前进度
	 * @return
	 */
	public int getProgCur();
	
	/**
	 * 获取当前正在安装的显示名
	 * @return
	 */
	public String getCurLabel();
	
	public void start();
	
	/**
	 * 
	 * @param waitForFinish 等待安装任务线程执行完毕，确保装机日志收集完毕
	 */
	public void stop(boolean waitForFinish);
}
