package com.edroid.droidhelper.pc.model;

import java.io.File;

/**
 * 程序全局数据
 * 
 * @author yichou 2015-1-8
 *
 */
public class AppGlobal {
	public static final String SERVER_URL = "http://192.168.1.230:8080/Doom";
	public static final String API_URL_LOGIN = SERVER_URL + "/business/login.action";
	public static final String API_URL_TOOLLOGS = SERVER_URL + "/log/saveToollogs.action";
	
	public static final String PREFS_NAME = "/droidhelper";
	
	public static final String PATH_ROOT = System.getProperty("user.dir");
	
	public static final String PATH_ADB = PATH_ROOT + File.separator + "adb" + File.separator + "adb.exe";
	public static final String PATH_DATA = PATH_ROOT + File.separator + "data";
	
	public static final String PATH_APPS = PATH_DATA + File.separator + "apks";
	public static final String PATH_TMP = PATH_DATA + File.separator + "tmp";
	public static final String PATH_SCRIPTS = PATH_DATA + File.separator + "scripts";
	
	public static final String PATH_LOG_DB =  "data\\tmp\\log-v1.db";

	public static final boolean DEBUG_ON = new File(PATH_ROOT, "debug").exists();
	
	public static String username, userid;
}
