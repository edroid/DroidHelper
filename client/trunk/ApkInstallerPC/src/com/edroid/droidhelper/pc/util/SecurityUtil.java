package com.edroid.droidhelper.pc.util;

import it.sauronsoftware.base64.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class SecurityUtil {
	static final Logger log = Logger.create("EncryptUtils");

	private static byte[] encryptKey;

	private static DESedeKeySpec spec;
	private static SecretKeyFactory keyFactory;
	private static SecretKey theKey;
	private static Cipher cipher;
	private static IvParameterSpec IvParameters;

	static {
		try {
			// 检测是否有 TripleDES 加密的供应程序
			// 如无，明确地安装SunJCE 供应程序
			try {
				Cipher.getInstance("DESede");
			} catch (Exception e) {
				log.e("no DESede");
				// System.err.println("Installling SunJCE provider.");
				// Provider sunjce = new com.sun.crypto.provider.SunJCE();
				// Security.addProvider(sunjce);
			}
			// 创建一个密钥
			encryptKey = "eedc6fgrfdertgajuhytbjksix".getBytes();
			// 为上一密钥创建一个指定的 DESSede key
			spec = new DESedeKeySpec(encryptKey);
			// 得到 DESSede keys
			keyFactory = SecretKeyFactory.getInstance("DESede");
			// 生成一个 DESede 密钥对象
			theKey = keyFactory.generateSecret(spec);
			// 创建一个 DESede 密码
			cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			// 为 CBC 模式创建一个用于初始化的 vector 对象
			// IvParameters = new IvParameterSpec(new byte[] { 12, 34, 56, 78,
			// 90, 87, 65, 43 });
			IvParameters = new IvParameterSpec(new byte[] { 1, 3, 5, 7, 0, 8, 6, 4 });
		} catch (Exception exc) {
			exc.printStackTrace();
			// 记录加密或解密操作错误
		}
	}

	/**
	 * 加密算法
	 *
	 * @param password
	 *            等待加密的密码
	 * @return 加密以后的密码
	 * @throws Exception
	 */
	public static String encrypt(String password) {
		if(password == null)
			return null;
		
		String encrypted_password = null;
		byte[] encrypted_pwd = null;

		try {
			// 以加密模式初始化密钥
			cipher.init(Cipher.ENCRYPT_MODE, theKey, IvParameters);
			// 加密前的密码（旧）
			byte[] plainttext = password.getBytes();
			// 加密密码
			encrypted_pwd = cipher.doFinal(plainttext);
			// 转成字符串，得到加密后的密码（新）
			encrypted_password = new String(Base64.encode(encrypted_pwd));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return encrypted_password;
	}

	/**
	 * 解密算法
	 *
	 * @param password
	 *            加过密的密码
	 * @return 解密后的密码
	 */
	public static String decrypt(String epassword) {
		if(epassword == null)
			return null;
		
		byte[] password = Base64.decode(epassword.getBytes());
		String decrypted_password = null;

		try {
			// 以解密模式初始化密钥
			cipher.init(Cipher.DECRYPT_MODE, theKey, IvParameters);
			// 构造解密前的密码
			byte[] decryptedPassword = password;
			// 解密密码
			byte[] decrypted_pwd = cipher.doFinal(decryptedPassword);
			// 得到结果
			decrypted_password = new String(decrypted_pwd);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return decrypted_password;
	}

//	public static void main(String args[]) {
//		long t0 = System.currentTimeMillis();
//
//		String str = "12345678";
//		String e = EncryptData.encrypt(str);
//		System.out.println(e);
//		System.out.println(EncryptData.decrypt(e));
//		System.out.println("time=" + (System.currentTimeMillis() - t0));
//	}
}
