package com.edroid.droidhelper.pc.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import com.edroid.droidhelper.pc.model.DeviceChangeListener;
import com.edroid.droidhelper.pc.model.Phone;

public class DetailsFrame extends JFrame implements DeviceChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 101L;
	
	private JScrollPane scrollPane2;
	private JPanel panel_1;
	private Label title;
	private JList<Phone> list_1;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JPanel controlPanel;
	private JLabel label_2;
	private JPanel screenPanel;
	private JPanel btnContiner;
	private JButton btnReboot;
	private JPanel progressContiner;
	private JLabel lblHead;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_6;
	private JLabel label_5;
	private JMenuBar menuBar;
	private JMenu menu;
	private JCheckBoxMenuItem checkBoxMenuItem;
	private JMenu menu_1;
	private JMenu mnAbout;
	private JToolBar toolBar;
	private JLabel label;
	private JTextField textField;
	private JButton btnLogin;
	private JPanel panel_2;
	private JLabel label_1;
	
	private JList<Phone> mDeviceList;
	private DefaultListModel<Phone> mListModel;
	private JLabel lblScreen;
	private JButton btnSave;
	private JButton btnRefresh;
	private JLabel lblModel;
	private JLabel lblFact;
	private JLabel lblImei;
	private JLabel lblProg;
	private JProgressBar progressBar;
	
	private int curSelectIndex = -1;
	

	public void DetailsFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/res/drawable/icon.png")));
		BorderLayout borderLayout = (BorderLayout) getContentPane().getLayout();
		
		setForeground(Color.GRAY);
		setTitle("亿卓装机助手");
		setSize(1024, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		splitPane.setBorder(BorderFactory.createEmptyBorder());
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		//设备列表
		{
			scrollPane2 = new JScrollPane();
			scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane2.setViewportBorder(BorderFactory.createEmptyBorder());
//			getContentPane().add(scrollPane2, BorderLayout.WEST);
			
			panel_1 = new JPanel(new BorderLayout());
			panel_1.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), new LineBorder(new Color(128, 128, 128))));
			
			title = new Label("Devices");
			title.setAlignment(Label.CENTER);
			panel_1.add(title, BorderLayout.NORTH);

			list_1 = new JList<Phone>();
			list_1.setBorder(new LineBorder(new Color(0, 0, 0)));
			list_1.setBackground(Color.WHITE);
//			list.setBorder(new CompoundBorder(new EmptyBorder(5, 5, 5, 5), new LineBorder(new Color(128, 128, 128))));
			list_1.setBorder(new EmptyBorder(5, 5, 5, 5));
			list_1.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					@SuppressWarnings("unchecked")
					JList<Phone> list = (JList<Phone>) e.getSource();
					int index = list.getSelectedIndex(); // 已选项的下标
					
					selectDevice(mListModel.get(index), index);
					
					if (e.getClickCount() == 2) {
						System.out.println("双击");
					}
				}
			});
			
			mListModel = new DefaultListModel<Phone>();
			list_1.setModel(mListModel);
			list_1.setCellRenderer(new DeviceListCellRender());
			mDeviceList = list_1;
			list_1.setSize(300, 0);
			
			panel_1.add(list_1, BorderLayout.CENTER);
			
			scrollPane2.setViewportView(panel_1);
			panel_1.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{list_1}));
			
			splitPane.setLeftComponent(scrollPane2);
		}
		
		{
			scrollPane = new JScrollPane();
//			scrollPane.setViewportBorder(new LineBorder(Color.PINK, 2, true));
//			getContentPane().add(scrollPane, BorderLayout.CENTER);
			splitPane.setRightComponent(scrollPane);
			
			BorderLayout bl_panel = new BorderLayout();
			panel = new JPanel(bl_panel);
			panel.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), new LineBorder(new Color(128, 128, 128))));
			
			panel.setMinimumSize(new Dimension(300, 0));

			controlPanel = new JPanel();
	
			label_2 = new JLabel("亿卓装机助手");
			controlPanel.add(label_2);
			panel.add(controlPanel, BorderLayout.NORTH);

			//屏幕截图模块
			{
				screenPanel = new JPanel();
				screenPanel.setBackground(Color.WHITE);
				screenPanel.setBorder(new EmptyBorder(8, 20, 5, 5));
				BoxLayout bl_screenPanel = new BoxLayout(screenPanel, BoxLayout.Y_AXIS);
				screenPanel.setLayout(bl_screenPanel);
				screenPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
				
				
				Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/res/drawable/demo.jpg"));
				img = img.getScaledInstance(400, -1, Image.SCALE_SMOOTH);
				
				lblScreen = new JLabel();
				lblScreen.setIcon(new ImageIcon(img));
				lblScreen.setAlignmentX(Component.CENTER_ALIGNMENT);
				screenPanel.add(lblScreen);
				
				//按钮条
				{
					btnContiner = new JPanel();
					btnContiner.setAlignmentY(Component.CENTER_ALIGNMENT);
					btnContiner.setAlignmentX(Component.CENTER_ALIGNMENT);
					btnContiner.setBackground(Color.WHITE);
					
					btnSave = new JButton("截图");
					btnContiner.add(btnSave);
					
					btnRefresh = new JButton("刷新");
					btnContiner.add(btnRefresh);

					btnReboot = new JButton("重启");
					btnContiner.add(btnReboot);
					
					screenPanel.add(btnContiner);
				}
				
				panel.add(screenPanel, BorderLayout.WEST);
			}

			
			//进度展示模块
			{
				progressContiner = new JPanel(new MigLayout("", "[][grow]", "[][][][][][][][]"));
				progressContiner.setBorder(new EmptyBorder(8, 5, 5, 5));
				progressContiner.setBackground(Color.WHITE);
				panel.add(progressContiner, BorderLayout.CENTER);
				
				lblHead = new JLabel();
				lblHead.setIcon(new ImageIcon(getClass().getResource("/res/drawable/android.png")));
				lblHead.setBounds(60, 40, 128, 128);
				progressContiner.add(lblHead, "cell 0 0,alignx trailing");
				
				
				panel_2 = new JPanel();
				panel_2.setBackground(Color.WHITE);
				progressContiner.add(panel_2, "cell 1 0,grow");
				panel_2.setLayout(new MigLayout());
				
				label_1 = new JLabel("用户名");
				panel_2.add(label_1);
				
				textField = new JTextField();
				textField.setColumns(12);
				panel_2.add(textField, "growx,wrap");

				label_1 = new JLabel("密码");
				panel_2.add(label_1);
				
				textField = new JTextField();
				panel_2.add(textField, "growx,wrap");

				btnLogin = new JButton("登陆");
				btnLogin.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					}
				});
				panel_2.add(btnLogin, "growx");
				
				int y = 220;
				label_3 = new JLabel("机型：");
				label_3.setBounds(45, y, 50, 20);
				progressContiner.add(label_3, "cell 0 2");
				lblModel = new JLabel("Nexus 5");
				lblModel.setBounds(120, y, 200, 20);
				lblModel.setForeground(Color.BLUE);
				progressContiner.add(lblModel, "cell 1 2");
				y += 25;
				
				label_4 = new JLabel("厂商：");
				label_4.setBounds(45, y, 50, 20);
				progressContiner.add(label_4, "cell 0 3");
				lblFact = new JLabel("LGE");
				lblFact.setBounds(120, y, 200, 20);
				lblFact.setForeground(Color.BLUE);
				progressContiner.add(lblFact, "cell 1 3");
				y += 25;
				
				label_6 = new JLabel("IMEI：");
				label_6.setBounds(45, y, 60, 20);
				progressContiner.add(label_6, "cell 0 4");
				lblImei = new JLabel("35409874748");
				lblImei.setBounds(120, y, 200, 20);
				lblImei.setForeground(Color.BLUE);
				progressContiner.add(lblImei, "cell 1 4");
				y += 80;
				
				label_5 = new JLabel("当前：");
				label_5.setBounds(45, y, 72, 20);
				progressContiner.add(label_5, "cell 0 5");
				lblProg = new JLabel("2/12");
				lblProg.setForeground(Color.GREEN);
				lblProg.setBounds(120, y, 72, 18);
				progressContiner.add(lblProg, "cell 1 5");
				y += 25;
				
				progressBar = new JProgressBar();
				progressBar.setBounds(45, y, 250, 14);
				progressContiner.add(progressBar, "cell 0 6 2 1,growx");
				
				
				
				
			}
			scrollPane.setViewportView(panel);
		}
		
		{
			menuBar = new JMenuBar();
			menuBar.setBorderPainted(false);
			getContentPane().add(menuBar, BorderLayout.NORTH);
			
			menu = new JMenu("文件");
			menuBar.add(menu);
			
			checkBoxMenuItem = new JCheckBoxMenuItem("自动保存");
			menu.add(checkBoxMenuItem);
			
			menu_1 = new JMenu("关于");
			menuBar.add(menu_1);
			
			mnAbout = new JMenu("about");
			menu_1.add(mnAbout);
			
			toolBar = new JToolBar();
			toolBar.setFloatable(false);
			getContentPane().add(toolBar, BorderLayout.SOUTH);
			
			label = new JLabel("时间：17：32");
			toolBar.add(label);
		}
		getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{splitPane, scrollPane2, panel_1, title, list_1, scrollPane, panel, controlPanel, label_2, screenPanel, lblScreen, btnContiner, btnSave, btnRefresh, btnReboot, progressContiner, lblHead, label_3, lblModel, label_4, lblFact, label_6, lblImei, label_5, lblProg, progressBar, menuBar, menu, checkBoxMenuItem, menu_1, mnAbout, toolBar, label}));
		
		
		
		setVisible(true);
	}
	
	private void selectDevice(Phone device, int index) {
		if(index == curSelectIndex)
			return;
		
		index = curSelectIndex;
		device.fetchScreenshot(new Phone.FetchScreenshotCallback() {
			
			@Override
			public void onSuc(Image image) {
				lblScreen.setIcon(new ImageIcon(image.getScaledInstance(400, -1, Image.SCALE_SMOOTH)));
			}
			
			@Override
			public void onFail(Exception e) {
				e.printStackTrace();
			}
		});
		lblFact.setText(device.getManufacturer());
		lblImei.setText(device.getImei());
		lblModel.setText(device.getModel());
	}

	@Override
	public void onDeviceAttached(Phone device) {
		mListModel.addElement(device);
	}

	@Override
	public void onDeviceDetached(Phone device) {
		mListModel.removeElement(device);
	}

	@Override
	public void onDeviceStateChanged(Phone device, boolean available) {
		// TODO Auto-generated method stub
		
	}
}
