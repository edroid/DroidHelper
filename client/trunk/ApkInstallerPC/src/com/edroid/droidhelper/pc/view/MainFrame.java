package com.edroid.droidhelper.pc.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import com.edroid.droidhelper.pc.model.AppGlobal;
import com.edroid.droidhelper.pc.model.DeviceChangeListener;
import com.edroid.droidhelper.pc.model.Phone;
import com.edroid.droidhelper.pc.model.Res;
import com.edroid.droidhelper.pc.util.Logger;
import com.edroid.droidhelper.pc.util.TimeUtils;

public class MainFrame extends JFrame implements DeviceChangeListener {
	static final Logger log = Logger.create(MainFrame.class.getSimpleName());
	
	private static final long serialVersionUID = 5L;
	
	private JPanel deviceContinerPanel, userPanel;
	private Map<Phone, DeviceRender> deviceMap;
	private JLabel lblDeviceCount, lblTime, lblCount, lblInfo;
	private Timer timer;
	
	
	private void exit() {
		log.w("exit----------------");
		
		mTask.cancel();
		timer.cancel();
		timer.purge();
		
		dispose();
		
		System.exit(0); // 关闭
	}
	
	private void logout() {
		timer.cancel();
		dispose();

		LoginDialog.loginOut();
	}
	
	public MainFrame() {
		deviceMap = new HashMap<Phone, DeviceRender>();

		setIconImage(Res.getImage("/res/drawable/icon.png"));
		setTitle("亿卓装机助手");
		setBounds(100, 100, 800, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		addWindowListener(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
				int a = JOptionPane.showConfirmDialog(null, "确定关闭吗？", "温馨提示", JOptionPane.YES_NO_OPTION);
				if (a == 0) {
					exit();
				}
			}
		});
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		{
			JMenu mnNewMenu = new JMenu("文件");
			menuBar.add(mnNewMenu);
			
			JMenuItem menuItem = new JMenuItem("退出");
			mnNewMenu.add(menuItem);
			
			JMenu mnNewMenu_1 = new JMenu("操作");
			menuBar.add(mnNewMenu_1);
			
			JMenuItem menuItem_1 = new JMenuItem("上传数据");
			mnNewMenu_1.add(menuItem_1);
			
			JMenu menu = new JMenu("帮助");
			menuBar.add(menu);
			
			JMenuItem menuItem_3 = new JMenuItem("使用帮助");
			menu.add(menuItem_3);
			
			JMenuItem menuItem_2 = new JMenuItem("关于我们");
			menuItem_2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new AboutDialog().setVisible(true);
				}
			});
			menu.add(menuItem_2);
		}
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		//内容
		{
			JPanel panel = new JPanel();
			contentPane.add(panel, BorderLayout.CENTER);
			panel.setLayout(new BorderLayout(0, 0));
			
			{
				JPanel topPanel = new JPanel(new MigLayout("", "[left][grow,left][right]", "[]"));
				topPanel.setBorder(null);
				panel.add(topPanel, BorderLayout.NORTH);

				JLabel label_1 = new JLabel("当前装机数：");
				topPanel.add(label_1, "cell 0 0");
				
				lblCount = new JLabel();
				lblCount.setForeground(new Color(51, 0, 255));
				topPanel.add(lblCount, "cell 1 0");
				
				{
					FlowLayout layout = new FlowLayout();
					userPanel = new JPanel(layout);
					topPanel.add(userPanel, "cell 2 0");
					
					JLabel label = new JLabel("欢迎您，");
					label.setHorizontalAlignment(SwingConstants.CENTER);
					userPanel.add(label);

					label_2 = new JLabel(AppGlobal.username);
					label_2.setForeground(new Color(255, 51, 255));
					userPanel.add(label_2);

					JButton button = new JButton("注销");
					button.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							logout();
						}
					});
					// button.setVisible(false);
					userPanel.add(button);
				}
			}

			deviceContinerPanel = new JPanel();
			deviceContinerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
			deviceContinerPanel.setLayout(new GridLayout(0, 4, 15, 15));
			
			JScrollPane scrollPane = new JScrollPane(deviceContinerPanel);
			panel.add(scrollPane);
		}
		
		//状态栏
		{
			JPanel statusPanel = new JPanel();
			statusPanel.setBorder(null);
			contentPane.add(statusPanel, BorderLayout.SOUTH);
			statusPanel.setLayout(new MigLayout("", "[left][left][grow,center][right][right]", "[18px]"));

			JLabel label = new JLabel("当前连接设备数：");
			statusPanel.add(label, "cell 0 0");
			
			lblDeviceCount = new JLabel();
			lblDeviceCount.setForeground(Color.BLUE);
			statusPanel.add(lblDeviceCount, "cell 1 0");
			
			lblInfo = new JLabel();
			lblInfo.setHorizontalAlignment(SwingConstants.CENTER);
			statusPanel.add(lblInfo, "cell 2 0,growx");
			
			JLabel label_1 = new JLabel("当前时间：");
			statusPanel.add(label_1, "cell 3 0");
			
			lblTime = new JLabel();
			lblTime.setForeground(Color.BLUE);
			lblTime.setText(TimeUtils.getTimeNow());
			statusPanel.add(lblTime, "cell 4 0");
		}
		
		startTimer();
	}
	
	public void setStateInfo(String text) {
		lblInfo.setText(text);
	}
	
	private final Runnable updateTimeRun = new Runnable() {
		
		@Override
		public void run() {
			System.out.println("---");
			lblTime.setText(TimeUtils.getTimeNow());
			lblDeviceCount.setText(String.valueOf(deviceMap.size()));
		}
	};
	
	private final TimerTask mTask = new TimerTask() {
		
		@Override
		public void run() {
			SwingUtilities.invokeLater(updateTimeRun);
		}
	};
	private JLabel label_2;
	
	private void startTimer() {
		timer = new Timer(true);
		timer.schedule(mTask, 1000, 1000);
	}
	
	@Override
	public void onDeviceAttached(Phone device) {
		final DeviceRender render = new DeviceRender(device);
		deviceMap.put(device, render);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				deviceContinerPanel.add(render);
			}
		});
	}

	@Override
	public void onDeviceDetached(Phone device) {
		final DeviceRender render = deviceMap.get(device);
		if(render != null) {
			deviceMap.remove(device);
			
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					deviceContinerPanel.remove(render);
				}
			});
		}
	}

	@Override
	public void onDeviceStateChanged(Phone device, boolean available) {
		final DeviceRender render = deviceMap.get(device);
		
		if(render != null && available) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					render.showDeviceInfo();
				}
			});
		}
	}
}
