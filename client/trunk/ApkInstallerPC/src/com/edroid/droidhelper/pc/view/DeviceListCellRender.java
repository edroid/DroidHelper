package com.edroid.droidhelper.pc.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import com.edroid.droidhelper.pc.model.Phone;

import net.miginfocom.swing.MigLayout;


public class DeviceListCellRender extends JPanel implements ListCellRenderer<Phone> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6L;
	static final Border lineBorder = BorderFactory.createLineBorder(new Color(0xFFDEAD), 1);
	static final Border emptyBorder = BorderFactory.createEmptyBorder(1, 1, 1, 1);

	static final Color clrSel = new Color(0xFFEFD5);
	static final Color clr = Color.WHITE;
	
	private JLabel name, prop;
	
	
	public DeviceListCellRender() {
		super(new MigLayout());
		
		setBorder(new EmptyBorder(10, 5, 10, 15));
		setBackground(Color.WHITE);
		
		JLabel icon = new JLabel(new ImageIcon(getClass().getResource("/res/drawable/ic_phone.png")));
		icon.setVerticalAlignment(SwingConstants.CENTER);
		add(icon, "dock west");
		
		name = new JLabel();
		name.setForeground(Color.BLACK);
		add(name, "wrap");
		
		prop = new JLabel("正在进行 1/12");
		prop.setForeground(Color.BLUE);
		add(prop);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Phone> list, Phone value, int index, 
			boolean isSelected, boolean cellHasFocus) {
		@SuppressWarnings("unchecked")
		DefaultListModel<Phone> model = (DefaultListModel<Phone>) list.getModel();
		Phone device = model.getElementAt(index);
		
		name.setText(device.toString());
		
		if (isSelected) {
			setBackground(clrSel);
		} else {
			setBackground(clr);
		}

		if (cellHasFocus)
			setBorder(lineBorder);
		else
			setBorder(emptyBorder);
		
		return this;
	}
}