package com.edroid.droidhelper.pc.model;

import com.android.ddmlib.IShellOutputReceiver;

public class ShellCommand implements IShellOutputReceiver {
	public interface CommandResultReceiver {
		
		public void onSuc(String ret);

		public void onFail(Exception e);
	}

	final StringBuilder sb = new StringBuilder(1024);
	String cmd;
	CommandResultReceiver r;
	boolean cancelled;
	
	
	public ShellCommand(String cmd, CommandResultReceiver r) {
		this.cmd = cmd;
		this.r = r;
	}
	
	public void cancel() {
		cancelled = true;
	}
	
	public String getCmd() {
		return cmd;
	}
	
	public void onException(Exception e) {
		r.onFail(e);
	}

	@Override
	public void addOutput(byte[] data, int offset, int length) {
		sb.append(new String(data, offset, length));
	}

	@Override
	public void flush() {
		r.onSuc(sb.toString());
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}
}