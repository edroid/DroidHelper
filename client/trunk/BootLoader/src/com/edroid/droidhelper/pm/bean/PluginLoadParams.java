package com.edroid.droidhelper.pm.bean;

/**
 * 插件加载参数
 * 
 * @author 建宾 2014-11-30
 *
 */
public final class PluginLoadParams {
	public String pluginName;
	public String pluginReqFlag;
	public String pluginEntryClassName;
	public int loadMode;
	
	
	public PluginLoadParams(String pluginName, String pluginReqFlag, String pluginEntryClassName, int loadMode) {
		this.pluginName = pluginName;
		this.pluginReqFlag = pluginReqFlag;
		this.pluginEntryClassName = pluginEntryClassName;
		this.loadMode = loadMode;
	}
}
