package com.edroid.droidhelper.pm.bean;

/**
 * 插件加载结果
 * 
 * @author 建宾 2014-11-30
 *
 */
public final class PluginLoadResults {
	public ClassLoader classLoader;
	public String path;
	public int ver;
}