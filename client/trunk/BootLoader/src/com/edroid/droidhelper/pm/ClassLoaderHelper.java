package com.edroid.droidhelper.pm;

import java.net.URL;
import java.net.URLClassLoader;



/**
 * dex 加载帮助工具
 * 
 * @author yichou 2014-10-23
 *
 */
public class ClassLoaderHelper {

	public static ClassLoader load(String path) throws Exception {
		return load(path, null);
	}
	
	/**
	 * 
	 * 加载一个 classes.dex，so路径 app_lib，dexopt 路径 app_dexopt
	 * 
	 * @param context
	 * @param path 包含 classes.dex 文件的jar包集合，多个以 : 分割
	 * @param parent 父 classLoader
	 * 
	 * @return
	 * @throws Exception 
	 */
	public static ClassLoader load(String path, ClassLoader parent) throws Exception {
		URL url = new URL("file:///" + path);
		
		return URLClassLoader.newInstance(new URL[] { url });
	}
}
