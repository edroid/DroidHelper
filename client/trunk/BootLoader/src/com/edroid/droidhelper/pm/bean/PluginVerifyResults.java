package com.edroid.droidhelper.pm.bean;

public class PluginVerifyResults {
	public String name;
	public int ver;
	
	@Override
	public String toString() {
		return "[name=" + name + ", ver=" + ver + "]"; 
	}
}
