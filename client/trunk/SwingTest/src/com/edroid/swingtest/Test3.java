package com.edroid.swingtest;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Test3 extends JApplet {
	public Test3() {
		Container contentPane = getContentPane();
		JPanel panel = new JPanel(new BorderLayout());
		
		JPanel controlPanel = new JPanel();
		JPanel canvas = new Canvas();

		canvas.setBorder(BorderFactory.createLineBorder(Color.black));

		controlPanel.add(new JLabel("Name:"));
		controlPanel.add(new JTextField(20));

		panel.add(controlPanel, BorderLayout.NORTH);
		panel.add(canvas, BorderLayout.CENTER);

		contentPane.add(panel);
	}
}

class Canvas extends JPanel {
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Dimension size = getSize();
		g.setColor(Color.black);
		g.drawLine(50, 50, size.width, size.height);
		g.drawArc(20, 40, 25, 25, 0, 290);
		g.drawString("A JPanel Used as a Canvas", 20, 20);
	}
}
