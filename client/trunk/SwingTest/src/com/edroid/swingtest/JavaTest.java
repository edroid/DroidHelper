package com.edroid.swingtest;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.Enumeration;

import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JavaTest {
	static final String[] UIS = {
		"OptionPane.font",
		"Menu.font",
		"Tree.font",
		"ScrollPane.font",
		"CheckBoxMenuItem.font",
		"TitledBorder.font",
		"Viewport.font",
		"MenuItem.acceleratorFont",
		"RadioButton.font",
		"Button.font",
		"CheckBox.font",
		"RadioButtonMenuItem.acceleratorFont",
		"TabbedPane.font",
		"TextField.font",
		"Slider.font",
		"TextArea.font",
		"Panel.font",
		"EditorPane.font",
		"CheckBoxMenuItem.acceleratorFont",
		"Table.font",
		"Menu.acceleratorFont",
		"ProgressBar.font",
		"ToggleButton.font",
		"DesktopIcon.font",
		"InternalFrame.titleFont",
		"PasswordField.font",
		"TableHeader.font",
		"FormattedTextField.font",
		"MenuBar.font",
		"ColorChooser.font",
		"RadioButtonMenuItem.font",
		"ComboBox.font",
		"TextPane.font",
		"List.font",
		"MenuItem.font",
		"Spinner.font",
		"ToolBar.font",
		"PopupMenu.font",
		"ToolTip.font",
		"Label.font"
	};
	
	public static void main(String[] args) {
		System.out.println(System.getProperty("user.dir", "c:\\abc"));
		
		System.out.println(System.getProperties());
		
		System.out.println(System.getProperty("java.io.tmpdir"));
		
//		System.out.println(System.setProperty("user.dir", "c:\\abc"));
	}
	
	public static void main2(String[] args) {
		String[] ss = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		for(String s : ss) {
			System.out.println(s);
		}
		
		long t0 = System.currentTimeMillis();
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while(keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object v = UIManager.get(key);
			
//			System.out.println(key + "=" + v);
			if(v instanceof FontUIResource)
				System.out.println("\"" + key + "\",");
			
//			if (UIManager.get(key) instanceof FontUIResource)
//				UIManager.put(key, fontRes);
		}
		System.out.println("time=" + (System.currentTimeMillis() - t0));
		
//		UIManager.getLookAndFeelDefaults()
//        	.put("defaultFont", new Font("Arial", Font.BOLD, 14));
		
		t0 = System.currentTimeMillis();
//		setFont(new FontUIResource(new Font("Arial", Font.BOLD, 14)));
		FontUIResource fnt = new FontUIResource(new Font("Arial", Font.BOLD, 14));
		
		for(String s : UIS) {
			UIManager.put("Button.font", fnt);
		}
		
		System.out.println("time=" + (System.currentTimeMillis() - t0));
	}
	
	static void setFont(FontUIResource fnt) {
		UIManager.put("Button.font", fnt);
		UIManager.put("ToggleButton.font", fnt);
		UIManager.put("RadioButton.font", fnt);
		UIManager.put("CheckBox.font", fnt);
		UIManager.put("ColorChooser.font", fnt);
		UIManager.put("ComboBox.font", fnt);
		UIManager.put("Label.font", fnt);
		UIManager.put("List.font", fnt);
		UIManager.put("MenuBar.font", fnt);
		UIManager.put("MenuItem.font", fnt);
		UIManager.put("RadioButtonMenuItem.font", fnt);
		UIManager.put("CheckBoxMenuItem.font", fnt);
		UIManager.put("Menu.font", fnt);
		UIManager.put("PopupMenu.font", fnt);
		UIManager.put("OptionPane.font", fnt);
		UIManager.put("Panel.font", fnt);
		UIManager.put("ProgressBar.font", fnt);
		UIManager.put("ScrollPane.font", fnt);
		UIManager.put("Viewport.font", fnt);
		UIManager.put("TabbedPane.font", fnt);
		UIManager.put("Table.font", fnt);
		UIManager.put("TableHeader.font", fnt);
		UIManager.put("TextField.font", fnt);
		UIManager.put("PasswordField.font", fnt);
		UIManager.put("TextArea.font", fnt);
		UIManager.put("TextPane.font", fnt);
		UIManager.put("EditorPane.font", fnt);
		UIManager.put("TitledBorder.font", fnt);
		UIManager.put("ToolBar.font", fnt);
		UIManager.put("ToolTip.font", fnt);
		UIManager.put("Tree.font", fnt);
	}

	public static void main1(String[] args) {
		// TODO Auto-generated method stub

		// Logger log = Logger.getLogger("demo");
		//
		// System.out.println("------------");
		// log.log(Level.ALL, "all", new NullPointerException("a"));

		Log log = LogFactory.getFactory().getInstance("aa");
		
//		log.s
		log.fatal("fatal");
		log.debug("debug");
		
		log.info("into one method");

		try {

			System.out.println(9 / 0);

		} catch (RuntimeException e) {

			log.error(e.getMessage());

		}

		log.info("out one method");

	}

}
