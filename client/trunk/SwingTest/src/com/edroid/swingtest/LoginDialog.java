package com.edroid.swingtest;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			LoginDialog dialog = new LoginDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	JProgressBar progressBar;
	int cur, total;
	
	final Runnable updateb = new Runnable() {
		
		@Override
		public void run() {
			progressBar.setValue(cur);
		}
	};

	/**
	 * Create the dialog.
	 */
	public LoginDialog() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JComboBox comboBox = new JComboBox();
			comboBox.setModel(new DefaultComboBoxModel(new String[] {"苏定方", "手动阀", "手动阀", "事实上", "算法额"}));
			comboBox.setBounds(14, 13, 108, 24);
			contentPanel.add(comboBox);
		}
		
		progressBar = new JProgressBar();
		progressBar.setBounds(74, 81, 307, 14);
		contentPanel.add(progressBar);
		progressBar.setMaximum(12);
		
		JButton btnStart = new JButton("start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {

					@Override
					public void run() {
						
						while(true) {
							if(++cur == 100)
								cur = 0;
							
//							SwingUtilities.invokeLater(updateb);
							progressBar.setValue(cur);
							
							try {
								Thread.sleep(40);
							} catch (InterruptedException e) {
							}
						}
					}
				}).start();
			}
		});
		btnStart.setBounds(164, 116, 113, 27);
		contentPanel.add(btnStart);
		
//		progressBar.setMaximum(12);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new Thread(new Runnable() {
					int count = 0;
					
					@Override
					public void run() {
						progressBar.setValue(count++);
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
			}
		});
		
		
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
