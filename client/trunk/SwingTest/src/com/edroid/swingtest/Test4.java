package com.edroid.swingtest;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Test4 extends JApplet {
	private Component glassPane = new CustomGlassPane();

	public void init() {
		JRadioButton rb = new JRadioButton();
		JButton button = new JButton("show glass pane");
		Container contentPane = getContentPane();

		contentPane.setLayout(new FlowLayout());
		contentPane.add(button);

		rb.setIcon(new ImageIcon("duke_standing.gif"));
		rb.setRolloverIcon(new ImageIcon("duke_waving.gif"));

		setGlassPane(glassPane);
		contentPane.add(rb);

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				glassPane.setVisible(true);
			}
		});
	}
}

class CustomGlassPane extends JPanel {
	private JButton button;
	private String displayString = "glass pane ... ";

	public CustomGlassPane() {
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				setVisible(false);
			}
		});
	}

	public void paintComponent(Graphics g) {
		Dimension size = getSize();
		FontMetrics fm = g.getFontMetrics();
		int sw = fm.stringWidth(displayString);
		int fh = fm.getHeight();

		g.setColor(Color.blue);

		for (int row = fh; row < size.height; row += fh)
			for (int col = 0; col < size.width; col += sw)
				g.drawString(displayString, col, row);
	}
}
