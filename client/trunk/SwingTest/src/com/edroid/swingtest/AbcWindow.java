package com.edroid.swingtest;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JToolBar;

import java.awt.BorderLayout;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class AbcWindow {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AbcWindow window = new AbcWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AbcWindow() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			
			public void run() {
				System.out.println("exit");
			}
		});
		
		initialize();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true) {
					System.out.println("--");
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("文件");
		menuBar.add(mnNewMenu);
		
		JMenuItem menuItem = new JMenuItem("退出");
		mnNewMenu.add(menuItem);
		
		JMenu mnNewMenu_1 = new JMenu("操作");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem menuItem_1 = new JMenuItem("上传数据");
		mnNewMenu_1.add(menuItem_1);
		
		JMenu menu = new JMenu("帮助");
		menuBar.add(menu);
		
		JMenuItem menuItem_3 = new JMenuItem("使用帮助");
		menu.add(menuItem_3);
		
		JMenuItem menuItem_2 = new JMenuItem("关于我们");
		menu.add(menuItem_2);
	}
}
