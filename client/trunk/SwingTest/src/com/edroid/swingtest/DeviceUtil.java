package com.edroid.swingtest;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Formatter;
import java.util.Locale;

public class DeviceUtil {

	public static String getOsName() {
		return System.getProperty("os.name").toLowerCase();
	}
	
	public static String getDeviceName() {
		return System.getenv("COMPUTERNAME").toLowerCase();// 获取计算机名  
	}
	
	private static String formatMac(byte [] mac) {
		String sMAC = "";
        Formatter formatter = new Formatter(Locale.getDefault());
		for (int i = 0; i < mac.length; i++) {
			formatter.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : "").toString();
		}
		sMAC = formatter.toString();
        formatter.close();
        return sMAC;
	}
	
	public static String getMac() {
		try {
	        NetworkInterface ni = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
	        
//	        Enumeration<InetAddress> e = ni.getInetAddresses();
//	        while (e.hasMoreElements()) {
//				InetAddress inetAddress = (InetAddress) e.nextElement();
//				System.out.println(inetAddress);
//				
//				System.out.println(formatMac(ni.getHardwareAddress()));
//			}
	        
//	        ni.getInetAddresses().nextElement().getAddress();
//	        ni.getHardwareAddress();
	        
	        return formatMac(ni.getHardwareAddress());
		} catch (Exception e) {
		}
		
		return "00-00-00-00-00-00";
	}
	
	public static String getIp() {
		try {
			return InetAddress.getLocalHost().getHostAddress(); 
		} catch (Exception e) {
		}
		
		return "0.0.0.0";
	}
	
//	public static void main(String[] argc) {
//		System.out.println(getDeviceName());
//		
//		String os = getOsName();
//		System.out.println(os);
//
//		System.out.println(getMac());
//		
//		System.out.println(getIp());
//	}
}
