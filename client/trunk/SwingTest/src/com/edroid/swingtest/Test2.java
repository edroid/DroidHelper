package com.edroid.swingtest;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.DefaultListModel;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.plaf.FontUIResource;

public class Test2 extends JApplet {
	private JList list = new JList();

	private String[] items = { "item one", "item two", "item three",
			"item four", "item five", "item six", "item seven", "item eight",
			"item nine", "item ten" };

	public void init() {
		Container contentPane = getContentPane();

		JPanel controlPanel = new ControlPanel(this, list);

		contentPane.add(controlPanel, BorderLayout.NORTH);
		contentPane.add(new JScrollPane(list), BorderLayout.CENTER);
		populateList();
	}

	public void populateList() {
		final DefaultListModel model = new DefaultListModel();

		for (int i = 0; i < items.length; ++i)
			model.addElement(items[i]);

		list.setModel(model);

		if (list.isShowing())
			list.revalidate();

		model.addListDataListener(new ListDataListener() {
			
			public void contentsChanged(ListDataEvent e) {
				showStatus("contents changed");
			}

			public void intervalRemoved(ListDataEvent e) {
				Object[] message = new Object[] {
						"Removed item at index " + e.getIndex0(), " ",
						"There are now " + model.getSize() + " items" };
				JOptionPane.showMessageDialog(Test2.this, message,
						"Items Removed", // title
						JOptionPane.INFORMATION_MESSAGE); // type
			}

			public void intervalAdded(ListDataEvent e) {
				showStatus("interval added");
			}
		});
	}
	
	static {
		try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    		
    		initGlobalFontSetting(new Font("微软雅黑", Font.PLAIN, 16));
        } catch (Throwable e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * 改全局字体
	 * @param fnt
	 */
	public static void initGlobalFontSetting(Font fnt) {
		FontUIResource fontRes = new FontUIResource(fnt);
		
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while(keys.hasMoreElements()) {
			Object key = keys.nextElement();
			if (UIManager.get(key) instanceof FontUIResource)
				UIManager.put(key, fontRes);
		}
	}
}

class ControlPanel extends JPanel {
	JButton remove = new JButton("remove selected items");
	JButton repopulate = new JButton("repopulate");

	public ControlPanel(final Test2 test2, final JList list) {
		add(remove);
		add(repopulate);

		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selected = list.getSelectedIndices();
				DefaultListModel model = (DefaultListModel) list.getModel();

				for (int i = 0; i < selected.length; ++i) {
					model.removeElementAt(selected[i] - i);
				}
			}
		});
		
		repopulate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				test2.populateList();
			}
		});
	}
}
