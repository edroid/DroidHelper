package com.edroid.swingtest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.edroid.droidhelper.pc.util.Logger;

public class HttpTest {
	static final Logger log = Logger.create("HttpUtils");
	
	public static final String CHARSET = HTTP.UTF_8;
	public static final String USERAGENT = "FsSDK-Android";

	
	public HttpTest() {
//		HttpClient client = 
		try {
			name();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static HttpClient getHttpClient() {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		
		return httpclient;
	}
	
	public static String post(String uri, NameValuePair... params) throws Exception {
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		for (NameValuePair p : params) {
			list.add(p);
		}

		HttpPost post = new HttpPost(uri);
		post.setEntity(new UrlEncodedFormEntity(list, CHARSET));

		return post(post);
	}
	
	public static String post(HttpPost post) throws Exception {
		HttpClient client = null;

		try {
			client = getHttpClient();
			HttpResponse rsp = client.execute(post);
			int code = rsp.getStatusLine().getStatusCode();

			if (code == HttpStatus.SC_OK) {
				HttpEntity entity = rsp.getEntity();
				String ret = EntityUtils.toString(entity, CHARSET);
				
				EntityUtils.consume(entity);
				
				return ret;
			} else {
				log.e("post: rsp status code error, code=" + code);
			}
		} finally {
			if (client != null)
				client.getConnectionManager().shutdown();
		}

		return null;
	}
	
	public void name() throws Exception {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		try {
		    HttpGet httpget = new HttpGet("http://www.baidu.com/");
		 
		    HttpResponse response = httpclient.execute(httpget);
		    HttpEntity entity = response.getEntity();
		 
		    System.out.println("Login form get: " + response.getStatusLine());
		    EntityUtils.consume(entity);
		 
		    System.out.println("Initial set of cookies:");
		    List<Cookie> cookies = httpclient.getCookieStore().getCookies();
		    if (cookies.isEmpty()) {
		        System.out.println("None");
		    } else {
		        for (int i = 0; i < cookies.size(); i++) {
		            System.out.println("- " + cookies.get(i).toString());
		        }
		    }
		 
		    HttpPost httpost = new HttpPost("http://192.168.1.230:8080/Ice/user/login.action");
		 
		    List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		    nvps.add(new BasicNameValuePair("user.username", "admin"));
		    nvps.add(new BasicNameValuePair("user.password", "admin123"));
		 
		    httpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
		 
		    response = httpclient.execute(httpost);
		    entity = response.getEntity();
		 
		    System.out.println("Login form get: " + response.getStatusLine());
		    System.out.println(EntityUtils.toString(entity));

		    EntityUtils.consume(entity);
		    
		 
//		    System.out.println("Post logon cookies:");
//		    cookies = httpclient.getCookieStore().getCookies();
//		    if (cookies.isEmpty()) {
//		        System.out.println("None");
//		    } else {
//		        for (int i = 0; i < cookies.size(); i++) {
//		            System.out.println("- " + cookies.get(i).toString());
//		        }
//		    }
		 
		} finally {
		    // When HttpClient instance is no longer needed,
		    // shut down the connection manager to ensure
		    // immediate deallocation of all system resources
		    httpclient.getConnectionManager().shutdown();
		}
	}
	
	public static void main(String[] args) {
//		new HttpTest();
		
		try {
			String ret = post("http://192.168.1.230:8080/Ice/user/login.action",
					new BasicNameValuePair("user.username", "admin"),
					new BasicNameValuePair("user.password", "admin123"));
			System.out.println(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
